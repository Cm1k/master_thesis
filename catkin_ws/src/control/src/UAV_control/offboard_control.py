#Copyright <2021> <Chris Bruun Mikkelsen>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AR$

#!/usr/bin/python
from control.msg import aruco_center
import rospy
from std_msgs.msg import Header
from geometry_msgs.msg import PoseStamped, Twist
from tf.transformations import quaternion_from_euler
from mavros_msgs.srv import CommandBool, SetMode
from uav_vel_controller import velocity_control
import tf
import csv


class uav_offb:
    def __init__(self):
        # -- Publishing -- #
        self.attitude_pub = rospy.Publisher('/mavros/setpoint_velocity/cmd_vel_unstamped', Twist, queue_size=1)
        self.pos_setpoint_pub = rospy.Publisher('mavros/setpoint_position/local', PoseStamped, queue_size=1)
        # -- Subscriber -- #
        rospy.Subscriber("aruco_center", aruco_center, self.aruco_center_callback)

        # -- Services -- #
        self.set_arming_srv = rospy.ServiceProxy('mavros/cmd/arming', CommandBool)
        self.set_mode_srv = rospy.ServiceProxy('mavros/set_mode', SetMode)
        self.rate = rospy.Rate(30)
        self.pos = PoseStamped()
        self.pos.pose.position.x = 0
        self.pos.pose.position.y = 0
        self.pos.pose.position.z = 3
        self.orientation = [1, 0, 0]
        self.attitude_target = Twist()
        self.uav_state = 0
        self.stop_pos = False
        self.stop_vel = False

        self.arucoBoard_center = [0, 0, 0, 0]
        self.PIDs = velocity_control()

        self.PIDs.dist_pidSetup(0.6 * 1., 0., (0.6 * 0.4) / 40, 1. / 60, (-2.0, 2.0), False, -2.9) #decreased
        self.PIDs.lat_pidSetup(0.6 * 4., 0.2, (3 * 4 * 0.1) / 40, 1. / 60, (-5.0, 5.0), False, -1) #increased
        self.PIDs.altitude_pidSetup(0.6 * 5., 0, (3 * 5 * 0.1) / 40, 1. / 60, (-5.0, 5.0), False, -1.5)

    def aruco_center_callback(self, data):
        """Callback from Detection script, when the marker is found,
        these data are used for pointing the UAV directly towards the marker center."""
        self.arucoBoard_center[0] = data.x
        self.arucoBoard_center[1] = data.y
        self.arucoBoard_center[2] = data.img_height
        self.arucoBoard_center[3] = data.img_width

    def vel_control(self):
        """Velocity control function. should be published at least at 2 hz for the offboard still works."""
        self.attitude_target.linear.z = 0
        self.attitude_target.linear.x = 0
        self.attitude_target.linear.y = 0

        self.attitude_target.angular.z = 1

        while not rospy.is_shutdown():
            self.attitude_pub.publish(self.attitude_target)

            try:  # prevent garbage in console output when thread is killed
                self.rate.sleep()
            except rospy.ROSInterruptException:
                pass

    def set_offboard_pos(self, x, y, z):
        self.pos.pose.position.x = x
        self.pos.pose.position.y = y
        self.pos.pose.position.z = z

    def send_pos(self):
        """
        Publishes positional commands to the UAV.
        Used only to imitate the UAV arriving at rooftop.
        :return: nothing
        """
        self.pos.header = Header()
        self.pos.header.frame_id = "base_footprint"

        while not rospy.is_shutdown() and not self.stop_pos:
            self.pos.pose.orientation.x = \
                quaternion_from_euler(self.orientation[0], self.orientation[1], self.orientation[2])[0]
            self.pos.pose.orientation.y = \
                quaternion_from_euler(self.orientation[0], self.orientation[1], self.orientation[2])[1]
            self.pos.pose.orientation.z = \
                quaternion_from_euler(self.orientation[0], self.orientation[1], self.orientation[2])[2]
            self.pos.pose.orientation.w = \
                quaternion_from_euler(self.orientation[0], self.orientation[1], self.orientation[2])[3]

            self.pos.header.stamp = rospy.Time.now()
            self.pos_setpoint_pub.publish(self.pos)

            try:  # prevent garbage in console output when thread is killed
                self.rate.sleep()
            except rospy.ROSInterruptException:
                pass

    def set_offboardMode(self):
        try:
            rospy.wait_for_service('mavros/cmd/arming', 30)
            rospy.wait_for_service('mavros/set_mode', 30)
            rospy.loginfo("ROS services are up")
            self.set_mode_srv(0, "OFFBOARD")
            self.set_arming_srv(True)
        except rospy.ROSException:
            raise self.failureException("msg")

    def setVelocity(self, lin_x=0, lin_y=0, lin_z=0, ang_x=0, ang_y=0, ang_z=0.0):
        if lin_x is not None:
            self.attitude_target.linear.x = -lin_x
        if lin_y is not None:
            self.attitude_target.linear.y = -lin_y
        if lin_z is not None:
            self.attitude_target.linear.z = -lin_z
        self.attitude_target.angular.x = ang_x
        self.attitude_target.angular.y = ang_y
        self.attitude_target.angular.z = ang_z

    @staticmethod
    def log_info(x, y, z, filename):
        """Logging information."""
        with open(filename, mode='a') as uav_attitude_file:
            attitude_writer = csv.writer(uav_attitude_file, delimiter=',')
            attitude_writer.writerow([x, y, z])

    def hover(self):
        x = 0
        y = 0
        z = 0
        aruco_to_world_tf = tf.TransformListener()
        while not rospy.is_shutdown() and not self.stop_vel:
            try:
                (test_tvec, test_rvec) = aruco_to_world_tf.lookupTransform('/world', '/aruco', rospy.Time(0))
                x = self.PIDs.update_distPID(test_tvec[2])
                y = self.PIDs.update_latPID(test_tvec[0])
                z = self.PIDs.update_altitudePID(test_tvec[1])
            except:
                pass
            self.setVelocity(x, y, z, 0, 0, -(self.arucoBoard_center[0] - (self.arucoBoard_center[2] / 2)) * 0.001)
            self.rate.sleep()
