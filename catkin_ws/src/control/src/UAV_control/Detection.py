#Copyright <2021> <Chris Bruun Mikkelsen>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AR$

#!/usr/bin/python
import rospy
from sensor_msgs.msg import CameraInfo, Image
from cv_bridge import CvBridge
import cv2
import numpy as np
import rospkg
import os
import yaml
from control.msg import pos
import time

import std_msgs.msg
from aruco_manipulator import aruco_det

rospack = rospkg.RosPack()


class controller:
    def __init__(self):
        # -- Node -- #
        rospy.init_node('detector')
        self.dt = 0

        # -- Subscribers -- #
        # rospy.Subscriber('/iris/camera_red_iris/camera_info', CameraInfo, self.cam_cal_info)
        # rospy.Subscriber('/iris/camera_red_iris/image_raw', Image, self.img_callback)

        rospy.Subscriber('/iris/usb_cam/camera_info', CameraInfo, self.cam_cal_info)
        rospy.Subscriber('/iris/usb_cam/image_raw', Image, self.img_callback)
        # / iris / usb_cam / image_raw

        # -- Publishers -- #
        self.board_detected_pub = rospy.Publisher('Board_detected', std_msgs.msg.Bool, queue_size=0)

        # -- Variables -- #
        self.aruco = aruco_det()
        self.ARUCO_PARAMETERS = cv2.aruco.DetectorParameters_create()
        self.aruco_dict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_6X6_250)
        self.board = cv2.aruco.GridBoard_create(
            markersX=4,
            markersY=6,
            markerLength=0.482,
            markerSeparation=0.01,
            dictionary=self.aruco_dict)
        self.img = self.board.draw(outSize=(800, 1100))
        self.img = cv2.copyMakeBorder(self.img, 40, 40, 0, 0, cv2.BORDER_CONSTANT, value=(255, 255, 255))
        cv2.imwrite("board.png", self.img)
        self.cam_param = 0
        self.sim_cam_param = 0
        self.bridge = CvBridge()
        self.cal_chooser = "AUTO"
        self.Aruco_pos = pos()
        while self.cam_param == 0 and self.cal_chooser == "AUTO":
            self.set_cam_param(self.cal_chooser)
        if self.cam_param == 0 and self.cal_chooser == "MAN":
            self.set_cam_param(self.cal_chooser)
        self.current_center = [0, 0]
        self.initial_image = False
        rospy.sleep(2)
        print("go")

    def img_callback(self, data):
        cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
        if not self.initial_image:
            self.initial_image = True
            gray = cv2.cvtColor(cv_image, cv2.COLOR_BGR2GRAY)
            self.aruco.board_center.img_width, self.aruco.board_center.img_height = gray.shape

        self.single_img(cv_image)

    def set_cam_param(self, chooser):
        # Manual camera parameters
        if chooser == "MAN":
            self.cam_param = self.getDistortionMatrix()
            print(self.cam_param)

        # Auto camera params
        if chooser == "AUTO":
            self.cam_param = self.sim_cam_param
            print(self.cam_param)

    @staticmethod
    def getDistortionMatrix():
        mtx = 0
        dist = 0
        stream = open(
            os.path.join(rospack.get_path("control"), "src", "vision", "test_matrix.yaml"), 'r')
        dicti = yaml.load(stream)
        for key, value in dicti.items():
            if key == "dist_coeff":
                # print (str(value))
                dist = np.asarray(value)
            if key == "camera_matrix":
                # print (str(value))
                mtx = np.asarray(value)

        return mtx, dist

    def cam_cal_info(self, data):
        if self.sim_cam_param == 0:
            D = data.D
            K = np.asarray(([data.K[0], data.K[1], data.K[2]],
                            [data.K[3], data.K[4], data.K[5]],
                            [data.K[6], data.K[7], data.K[8]]))
            print(K, D)
            self.sim_cam_param = K, D

    def single_img(self, image):
#        print(1/(time.time() - self.dt))
        self.dt = time.time()
        K, D = self.cam_param
        undistort = cv2.undistort(image, K, D)
        aruco_detect, board_detected = self.aruco.ArUco_board_detection(undistort, K, D, self.aruco_dict,
                                                                        self.ARUCO_PARAMETERS, self.board)
        self.board_detected_pub.publish(board_detected)

        cv2.namedWindow('ArUco detecion', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('ArUco detecion', 480, 480)
        cv2.imshow("ArUco detecion", aruco_detect)
        cv2.waitKey(1)
    @staticmethod
    def run():
        while not rospy.is_shutdown():
            pass


def main():
    UAV = controller()
    UAV.run()


main()
