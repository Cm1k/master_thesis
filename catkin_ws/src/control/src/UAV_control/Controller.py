#Copyright <2021> <Chris Bruun Mikkelsen>
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE AR$
#!/usr/bin/python
from control.msg import pos, aruco_center
import rospy
import numpy as np
from std_msgs.msg import Header
from threading import Thread
from geometry_msgs.msg import PoseStamped, Twist
from mavros_msgs.srv import CommandBool, SetMode
import csv
import tf
from simple_pid import PID
import std_msgs.msg
from graphing import plotting
import time
# GPS in mavros: /mavros/global_position/raw/fix
import mavros_msgs.msg
from offboard_control import uav_offb
from state_machine import UAV_stateMachine


class attitudectl:
    def __init__(self):
        # -- Node -- #
        rospy.init_node("uav_controller")

        # -- Variables -- #
        self.plot = plotting()
        self.stateMachine = UAV_stateMachine()
        self.uav_offboard = uav_offb()
        self.accuracy_list = []
        self.uav_state = 0

        self.current_aruco_pos = pos()
        self.rate = rospy.Rate(60)
        self.uav_current_pos = PoseStamped()
        self.arucoBoard_center = [0, 0, 0, 0]
        self.Detected_marker = False
        self.plotstarted = False
        self.state = mavros_msgs.msg.State()

        # -- Subscribers -- #
        rospy.Subscriber("/ArUco_pos", pos, self.aruco_pos_callback)
        rospy.Subscriber("/mavros/local_position/pose", PoseStamped, self.uav_pos_callback)
        rospy.Subscriber("/Board_detected", std_msgs.msg.Bool, self.board_detection_callback)
        rospy.Subscriber("aruco_center", aruco_center, self.aruco_center_callback)
        rospy.Subscriber("/mavros/state", mavros_msgs.msg.State, self.state_callback)

        # -- Publishers -- #
        self.pos_setpoint_pub = rospy.Publisher('mavros/setpoint_position/local', PoseStamped, queue_size=1)
        self.attitude_pub = rospy.Publisher('/mavros/setpoint_velocity/cmd_vel_unstamped', Twist, queue_size=1)

        # -- Services -- #
        self.set_arming_srv = rospy.ServiceProxy('mavros/cmd/arming', CommandBool)
        self.set_mode_srv = rospy.ServiceProxy('mavros/set_mode', SetMode)

        rospy.sleep(1)
        rospy.loginfo("UAV Initated.")

    def state_callback(self, data):
        self.state = data

    def aruco_center_callback(self, data):
        """Callback from Detection script, when the marker is found,
        these data are used for pointing the UAV directly towards the marker center."""
        self.arucoBoard_center[0] = data.x
        self.arucoBoard_center[1] = data.y
        self.arucoBoard_center[2] = data.img_width
        self.arucoBoard_center[3] = data.img_height

    def uav_pos_callback(self, data):
        """Used to imitate the UAV arriving at the rooftop where the marker is placed"""
        self.uav_current_pos = data
        self.plot.setNewValues(data.pose.position.x, data.pose.position.y, data.pose.position.z)

    def board_detection_callback(self, data):
        """Tells the UAV if the marker board are detected or not."""
        self.Detected_marker = data.data

    def aruco_pos_callback(self, data):
        """Used to determine the UAVs ground truth position.
        Only used for documentation posibilities."""
        self.current_aruco_pos = data

    def log_info(self, x, y, z, filename):
        try:
            if 103 > len(list(open(filename))):
                with open(filename, mode='a') as uav_attitude_file:
                    attitude_writer = csv.writer(uav_attitude_file, delimiter=',')
                    attitude_writer.writerow([x, y, z])
            else:
                return 0
        except IOError:
            with open(filename, mode='a') as uav_attitude_file:
                attitude_writer = csv.writer(uav_attitude_file, delimiter=',')
                attitude_writer.writerow([self._get_setup("pitch")])
                attitude_writer.writerow([self._get_setup("roll")])
                attitude_writer.writerow([self._get_setup("altitude")])
                attitude_writer.writerow([x, y, z])
                print("Files does not exist, adding setup.")
        return 1

    def log_hover(self, x, y, z, filename):
        with open(filename, mode='a') as uav_attitude_file:
            attitude_writer = csv.writer(uav_attitude_file, delimiter=',')
            attitude_writer.writerow([x, y, z])

    def at_pos(self, x, y, z):
        """A check used to see if the UAV are at the desired position."""
        test_array_current_pos = np.array([self.uav_current_pos.pose.position.x, self.uav_current_pos.pose.position.y,
                                           self.uav_current_pos.pose.position.z])
        if np.allclose(np.array([x, y, z]), test_array_current_pos, rtol=0.5, atol=0.5):
            return True
        else:
            return False

    def _get_setup(self, component):
        if component == "pitch":
            return self.stateMachine.uav_offboard.PIDs.pitch_tunes
        if component == "roll":
            return self.stateMachine.uav_offboard.PIDs.roll_tunes
        if component == "altitude":
            return self.stateMachine.uav_offboard.PIDs.altitude_tunes

    @staticmethod
    def _getAverage(measurements, measList, listsize):
        measList[0].append(measurements[0])
        measList[1].append(measurements[1])
        measList[2].append(measurements[2])
        if len(measList[0]) > listsize:
            measList[0].pop(0)
        if len(measList[1]) > listsize:
            measList[1].pop(0)
        if len(measList[2]) > listsize:
            measList[2].pop(0)
        if len(measList[0]) < listsize - 1 and len(measList[1]) < listsize - 1 and len(measList[2]) < listsize - 1:
            return [0.0, 0.0, 0.0]
        return [sum(measList[0]) / listsize, sum(measList[1]) / listsize, sum(measList[2]) / listsize]

    @staticmethod
    def isclose(a, b, rel_tol=1e-9, abs_tol=0.0):
        return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

    def run(self):
        """Main function to keep the program running."""
        state = self.stateMachine.switch(0)
        self.uav_offboard.set_offboardMode()
        aruco_to_world_tf = tf.TransformListener()
        rolling_average_list = [[], [], []]
        means = [0.0, 0.0, 0.0]
        test_tvec = [0.0, 0.0, 0.0]
        capture = False
        start = time.time()

        # Main loop
        while not rospy.is_shutdown():
            try:
                (test_tvec, test_rvec) = aruco_to_world_tf.lookupTransform('/world', '/aruco', rospy.Time(0))
                means = self._getAverage(test_tvec, rolling_average_list, 100)
            except:
                pass

            if capture and state == 7 and self.uav_current_pos.pose.position.z < 0.1:
                rospy.sleep(2)
                if not self.log_info(self.uav_current_pos.pose.position.x, self.uav_current_pos.pose.position.y,
                                     self.uav_current_pos.pose.position.z,
                                     "/home/chris/school/master_thesis/tests/15-01-2021/3m_ZN_windy_4_0MS_GUSTTEST.csv"):
                    print("Test over")
                    break
                else:
                    break

            if state == 6 and self.isclose(a=means[2], b=-3.02639692, abs_tol=0.1) and self.isclose(a=test_tvec[2],
                                                                                                     b=-3.02639692,
                                                                                                     abs_tol=0.1) and self.isclose(
                a=means[0], b=-0.94943396, abs_tol=0.05) and self.isclose(a=test_tvec[0], b=-0.94943396,
                                                                          abs_tol=0.05):
                capture = True
                rospy.sleep(4)
                # state = self.stateMachine.switch(7)

            if state == 5: #and self.isclose(a=means[0], b=-0.94943396, abs_tol=0.2) and self.isclose(a=test_tvec[0],
                           #                                                                          b=-0.94943396,
                           #                                                                          abs_tol=0.2):
                rospy.sleep(4)
                state = self.stateMachine.switch(6)

            if state == 4: #and self.isclose(a=means[1], b=-1.5, abs_tol=0.1) and self.isclose(a=test_tvec[1], b=-1.5,
                           #                                                                   abs_tol=0.1):
                rospy.sleep(2)
                state = self.stateMachine.switch(5)

            if state == 3 and self.Detected_marker:
                rospy.sleep(2)
                state = self.stateMachine.switch(4)

            if state == 2 and self.Detected_marker:
                state = self.stateMachine.switch(3)
                rospy.sleep(2)

            # Rotate to locate marker
            if state == 1 and not self.Detected_marker: # and self.at_pos(0, 10, 2):'''
                rospy.sleep(2)
                state = self.stateMachine.switch(2)

            # Go to marker position to search for said marker
            if state == 0 and self.at_pos(0, 0, 3):
                state = self.stateMachine.switch(1)

            self.rate.sleep()
            # if time.time() - start > 300:
            #     break
            # if state > 5:
            #     self.log_hover(self.uav_current_pos.pose.position.x, self.uav_current_pos.pose.position.y, self.uav_current_pos.pose.position.z, "/home/chris/school/master_thesis/tests/20-01-2021/hovertest1.csv")
        rospy.sleep(1)


def main():
    uav = attitudectl()
    uav.run()


main()
