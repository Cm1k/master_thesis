clc;
clear;
m = csvread("20-01-2021/hovertest1.csv");
y = m(:,1);
x = m(:,2);
x = x-10
%help plot
plot(x,y)
hold on
axis equal
x = 0.05 * cos(0 : 0.01 : 2*pi);
y = 0.05 * sin(0 : 0.01 : 2*pi);
plot(x,y, 'g-', 'LineWidth', 2)

x = 0.1 * cos(0 : 0.01 : 2*pi) + 0;
y = 0.1 * sin(0 : 0.01 : 2*pi);
plot(x,y, 'y-', 'LineWidth', 2)

x = 0.2 * cos(0 : 0.01 : 2*pi) + 0;
y = 0.2 * sin(0 : 0.01 : 2*pi);
plot(x,y, 'r-', 'LineWidth', 2)

title('5 min hover test with wind Velocity Mean = 4.0')
xlabel('meters')
legend('Trajectory', '+- 5 cm', '+- 10 cm', '+-20 cm')
hold off

% Approach v1:
%roll_pid = PID(2, 0.3, 0.5, setpoint=-1)
%roll_pid.sample_time = 0.005
%roll_pid.output_limits = (-0.4, 0.4)
%roll_pid.auto_mode = False

% Approach v2:
%roll_pid = PID(1, 0.1, 0.2, setpoint=-1)
%roll_pid.sample_time = 0.005
%roll_pid.output_limits = (-0.4, 0.4)
%roll_pid.auto_mode = False

% Approach v3:
%roll_pid = PID(1, 0.1, 0.2, setpoint=-1)
%roll_pid.sample_time = 0.005
%roll_pid.output_limits = (-0.4, 0.4)
%roll_pid.auto_mode = False