%plot_marker_pos
clc
clear
m = csvread("setpoints.csv");
y = m(:,1);
x = m(:,3);
x = x-10
hold on
plot(x, y, '.-')
plot(x(1), y(1), '*')
hold off