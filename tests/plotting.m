%% V1 with markerLength=0.473
clc
clear
m = csvread("08-12-2020/ziegler_nichols1.csv");
%subplot(2,1,1)
hold on
xlabel m
y = m(:,2)
x = m(:,1)
y = y-10
x_1 = mean(y)
scatter(x,y,20, 'black', 'filled')
title("PD Controller p gain 0.3 d gain 0.3")
axis equal
xlabel('meters')
ylabel('meters')
%scatter(x,y, 'r-', 'LineWidth', 3)
x = 0.05 * cos(0 : 0.01 : 2*pi) + 0;
y = 0.05 * sin(0 : 0.01 : 2*pi) + 0;
plot(x,y, 'g-', 'LineWidth', 1)

x = 0.1 * cos(0 : 0.01 : 2*pi) + 0;
y = 0.1 * sin(0 : 0.01 : 2*pi) + 0;
plot(x,y, 'y-', 'LineWidth', 2)


x = 0.2 * cos(0 : 0.01 : 2*pi) + 0;
y = 0.2 * sin(0 : 0.01 : 2*pi) + 0;
plot(x,y, 'r-', 'LineWidth', 3)
legend('Individual landings',  '+- 5 cm', '+- 10 cm', '+-20 cm')
hold off

%% V2 with markerLength=0.480
clear
clc
%m = csvread("landingv1.csv"); %Figur 21
%m = csvread("landing_02-10.csv"); %Figur 24
%m = csvread("07-12-2020/(0,3.0.0,3.nowind).csv"); %Figur 22
%m = csvread("08-01-2021/ziegler_nichols_windyV1.csv", 3, 0); % Figur 26
%m = csvread("08-12-2020/ziegler_nichols3.csv", 3, 0); % Figur 25
m = csvread("15-01-2021/3m_ZN_windy_4_0MS.csv", 3, 0); % Figur 25
x = m(:,2)
x = x-10
y = m(:,1)
x_1 = mean(x)
x_2 = std(x)
%x_3 = var(x)
y_1 = mean(y)
y_2 = std(y)
%y_3 = var(y)

%plot mean
%scatter(x_1,y_1,100, 'blue', 'filled')

c1 = x_1 - .005
c2 = x_1 + .005

v1 = y_1 - .005
v2 = y_1 + .005
hold on


%subplot(2,1,2)
a = scatter(x,y,10, 'k', 'filled')
title("Ziegler Nichols no wind applied")
%title("V2 optimized PD values")
xlabel('meters')
ylabel('meters')
axis equal
%scatter(x,y, 'r-', 'LineWidth', 3)
x = 0.05 * cos(0 : 0.01 : 2*pi) + 0;
y = 0.05 * sin(0 : 0.01 : 2*pi) + 0;
b = plot(x,y, 'g-', 'LineWidth', 1);

x = 0.1 * cos(0 : 0.01 : 2*pi) + 0;
y = 0.1 * sin(0 : 0.01 : 2*pi) + 0;
c = plot(x,y, 'r-', 'LineWidth', 2);



e = plot([c1 c2],[y_1 y_1], 'b-', 'LineWidth', 2);
f = plot([x_1, x_1],[v1,v2], 'b-', 'LineWidth', 2);
e.Annotation.LegendInformation.IconDisplayStyle = 'off';
f.Annotation.LegendInformation.IconDisplayStyle = 'off';

t = linspace(0,2*pi) ;
a = x_2 ; b = y_2 ;
x = a*cos(t) ;
y = b*sin(t) ;
f = plot(x+x_1,y+y_1,'b--', 'LineWidth', 2);
%f.Annotation.LegendInformation.IconDisplayStyle = 'off';
legend('Datapoint', '+- 5 cm', '+- 10 cm', 'one standard deviation');

hold off
%%

diff = abs(x_1) + abs(x_2) / x_1

(diff * 0.007) + 0.473
