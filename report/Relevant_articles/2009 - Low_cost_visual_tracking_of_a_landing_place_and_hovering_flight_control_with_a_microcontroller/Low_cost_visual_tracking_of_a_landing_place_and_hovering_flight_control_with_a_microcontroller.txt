this article from 2009 describes a way to hover above four infrared lights with a Wii camera. which integrated circuit gives a pixel position of each tracked infrared light with high frequency.

The camera is placed on the bottom of a small UAV.
they do indoor experiments hovering 50 cm and 1 meter above a landing pad.
the UAV hovers with a RMS deviation of 1.7 cm.

