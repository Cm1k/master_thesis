Skralde artikel, dårlige figurer og svær at forstå.

This articles main focus is doing image segmentation to detect a T object on a ship.
The camera is a thermal imager, and the T's thermal radiation is higher than the background, and that is how they will segment the T from the image.
They use a automatic threshold segmentation, and after that, they use Sobel method to detect edges of the T.

