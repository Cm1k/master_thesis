<map version="freeplane 1.8.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Speciale" FOLDED="false" ID="ID_35344033" CREATED="1605109329044" MODIFIED="1605525474653" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="2.0">
    <properties fit_to_viewport="false" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="13" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Introduction" FOLDED="true" POSITION="right" ID="ID_666537596" CREATED="1605109357911" MODIFIED="1605525684780" HGAP_QUANTITY="15.499999955296518 pt" VSHIFT_QUANTITY="-22.499999329447768 pt">
<edge COLOR="#ff0000"/>
<node TEXT="What have other done" ID="ID_894118863" CREATED="1605109492979" MODIFIED="1605525674304"/>
<node TEXT="state of the art" ID="ID_1083871723" CREATED="1605109513677" MODIFIED="1605525450441" HGAP_QUANTITY="13.250000022351742 pt" VSHIFT_QUANTITY="-0.7499999776482589 pt"/>
<node TEXT="Project aim" FOLDED="true" ID="ID_1099363092" CREATED="1605526007592" MODIFIED="1605526010290">
<node TEXT="Camera placement" ID="ID_290485760" CREATED="1605526021295" MODIFIED="1605526026644"/>
<node TEXT="Communication (Why mavros)" ID="ID_1479513610" CREATED="1605526193730" MODIFIED="1605526203284"/>
</node>
<node TEXT="Motivation" FOLDED="true" ID="ID_244169212" CREATED="1605109422676" MODIFIED="1605109492285">
<node TEXT="OUH landings" ID="ID_1405760735" CREATED="1605525728546" MODIFIED="1605525732586"/>
</node>
<node TEXT="Materials" FOLDED="true" ID="ID_1314928103" CREATED="1605525356719" MODIFIED="1605525571158">
<node TEXT="Drone kit + camera" ID="ID_1950057322" CREATED="1605525698178" MODIFIED="1605525702365"/>
</node>
<node TEXT="Project design process" FOLDED="true" ID="ID_301671886" CREATED="1605525380568" MODIFIED="1605525571175">
<node TEXT="Trello" ID="ID_550084005" CREATED="1605525704296" MODIFIED="1605525705793"/>
<node TEXT="Weekly sprints" ID="ID_1130349662" CREATED="1605525706285" MODIFIED="1605525722352"/>
</node>
</node>
<node TEXT="Acknowledgements" FOLDED="true" POSITION="right" ID="ID_861398895" CREATED="1605525309581" MODIFIED="1605525825138">
<edge COLOR="#00007c"/>
<node TEXT="Henrik!" ID="ID_271784158" CREATED="1605525794388" MODIFIED="1605525797472"/>
</node>
<node TEXT="Methodology" POSITION="right" ID="ID_6115353" CREATED="1605109386631" MODIFIED="1605525645295">
<edge COLOR="#0000ff"/>
<node TEXT="Simulation" FOLDED="true" ID="ID_770150201" CREATED="1605526105313" MODIFIED="1605526108651">
<node TEXT="Gazebo" FOLDED="true" ID="ID_248301684" CREATED="1605526112809" MODIFIED="1605526115572">
<node TEXT="Wind setup" ID="ID_1576276562" CREATED="1605526415575" MODIFIED="1605526417976"/>
</node>
<node TEXT="V-REP" ID="ID_783846609" CREATED="1605526116170" MODIFIED="1605526130193"/>
<node TEXT="Webots" ID="ID_650458663" CREATED="1605526136700" MODIFIED="1605526138302"/>
</node>
<node TEXT="Communication" ID="ID_1142178814" CREATED="1605526143812" MODIFIED="1605526150079">
<node TEXT="ROS" FOLDED="true" ID="ID_1600798039" CREATED="1605526153256" MODIFIED="1605526155576">
<node TEXT="Node walkthrough" ID="ID_1814393694" CREATED="1605526164989" MODIFIED="1605526175805"/>
</node>
<node TEXT="MAVROS / MAVLINK" ID="ID_583106125" CREATED="1605526178836" MODIFIED="1605526186803"/>
</node>
<node TEXT="Vision" ID="ID_1087768979" CREATED="1605537839797" MODIFIED="1605537841667">
<node TEXT="Camera model" ID="ID_1065003215" CREATED="1605526219267" MODIFIED="1605526221660"/>
<node TEXT="Camera calibration" ID="ID_1704709265" CREATED="1605525933382" MODIFIED="1605525940197"/>
<node TEXT="Fiducial Markers" FOLDED="true" ID="ID_1933728707" CREATED="1605525740426" MODIFIED="1605526272102">
<node TEXT="Why ArUco" ID="ID_1951461962" CREATED="1605526259097" MODIFIED="1605526263613"/>
<node TEXT="Marker detection" ID="ID_1374909579" CREATED="1605526255472" MODIFIED="1605526258523"/>
</node>
</node>
<node TEXT="Regulering" FOLDED="true" ID="ID_572899531" CREATED="1605525757365" MODIFIED="1605525911453">
<node TEXT="P I D Controllers" ID="ID_862943220" CREATED="1605526091704" MODIFIED="1605526096765"/>
</node>
</node>
<node TEXT="Results" POSITION="right" ID="ID_334862892" CREATED="1605109408639" MODIFIED="1605525649064">
<edge COLOR="#00ff00"/>
</node>
<node TEXT="Discussion" POSITION="right" ID="ID_171811758" CREATED="1605109411719" MODIFIED="1605525651531">
<edge COLOR="#ff00ff"/>
</node>
<node TEXT="Conclusion" POSITION="right" ID="ID_132963595" CREATED="1605109414831" MODIFIED="1605525654486">
<edge COLOR="#00ffff"/>
</node>
<node TEXT="Perspectivation" POSITION="right" ID="ID_1711855866" CREATED="1605526078028" MODIFIED="1605526083485">
<edge COLOR="#0000ff"/>
</node>
</node>
</map>
